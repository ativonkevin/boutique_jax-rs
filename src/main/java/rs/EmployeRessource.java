package rs;

import entites.Categorie;
import entites.Employe;
import java.time.LocalDate;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import service.EmployeService;

/**
 *
 * @author Etherno
 */
@Path("/employe")
public class EmployeRessource {
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void ajouter(Employe e) {
        // ajoute l'objet e dans la collection liste
        EmployeService.ajouter(e);
        System.out.println("Ajout réussi");
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    // remplace par e, l'objet Employee de la liste qui a même id que e
    public void modifier(Employe e) {
        
        if(e == null){
            throw new IllegalArgumentException("Champ categorie vide");
        }
        
        EmployeService.modifier(e);
        System.out.println("Ajout réussi");
    }

    @GET
    @Path("/{id}")
    // renvoie l'objet Categorie de la liste qui a l'id passé en paramètre
    public Employe trouver(@PathParam("id") Long id) {
        
        if(id == null){
            throw new IllegalArgumentException("Champ vide");
        }
        return EmployeService.trouver(id);
    }
    
    @DELETE
    @Path("/{id}")
    // retirer de la liste, l'objet Categorie qui a l'id passé en paramètre
    public void supprimer(@PathParam("id") Long id) {
        
        if(id == null){
            throw new IllegalArgumentException("Champ id vide");
        }
        EmployeService.supprimer(id);
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/delete")
    // retirer de la liste, l'objet Categorie passé en paramètre
    public void supprimer(Employe e) {
        
        if(e == null){
            throw new IllegalArgumentException("Informations indisponibles");
        }        
        EmployeService.supprimer(e.getId());
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Employe> lister() {
        return EmployeService.lister();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getbyparams")
    public List<Employe> lister(@QueryParam("debut") int debut,@QueryParam("nombre") int nombre) {
        return EmployeService.lister(debut, nombre);
    }
    
}
