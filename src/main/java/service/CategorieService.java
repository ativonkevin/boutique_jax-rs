/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entites.Categorie;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Etherno
 */
public class CategorieService {
    static List<Categorie> liste = new ArrayList<>();
    
    //Constructeur
    public CategorieService(){}
    
    //Ajout d'un objet à la liste
    public static void ajouter(Categorie e){
        if(e == null){
            throw new IllegalArgumentException("Objet vide");
        }
        else{
            liste.add(e);
        }      
    }
    
    //Remplacer par e, l'objet Categorie de la liste qui a même id que e
    public static void modifier(Categorie e){
        if(e == null){
            throw new IllegalArgumentException("Objet vide");
        }
        else{
            int j=-1;
            for(int i=0; i<liste.size();i++){
                if(Objects.equals(liste.get(i).getId(), e.getId())){
                    j = i;
                    liste.set(j, e);
                }else
                    throw new IllegalArgumentException("Objet non trouvé");
            }
        }
    }
    
    //Renvoyer l'objet Categorie de la liste qui a l'id passé en paramètre
    public static Categorie trouver(Integer id){
        int position=-1;
        for(int i=0; i<liste.size();i++){
            if(Objects.equals(liste.get(i).getId(), id)){
                position = i;
            }else{
                throw new IllegalArgumentException("Objet non trouvé");
            }
        }
        return liste.get(position);
    }
    
    //retirer de la liste, l'objet Categorie qui a l'id passé en paramètre
    public static void supprimer(Integer id){
        int pos=-1;
        for (int i=0; i<liste.size(); i++) {
            if(Objects.equals(liste.get(i).getId(), id)){
                pos = i;
            }
            else{
                throw new IllegalArgumentException("Objet non trouvé");
            }
        }
        liste.remove(pos);
    }
    
    //retirer de la liste, l'objet Categorie passé en paramètre
    public static void supprimer(Categorie e){
        supprimer(e.getId());
    }
    
    //renvoyer tous les éléments de la liste
    public static List<Categorie> lister(){
        if(liste == null){
            throw new IllegalArgumentException("Liste vide");
        }
        return liste;
    }
    
    // renvoyer nombre éléments de la liste, commençant à la position debut
    public static List<Categorie> lister(int debut, int nombre){
        if(liste == null){
            throw new IllegalArgumentException("Liste vide");
        }
        if(debut>liste.size() || nombre < 0){
            throw new IllegalArgumentException("Parametes inavalides");
        }
        List<Categorie> listeNouvel;
        listeNouvel = new ArrayList();
        int taille = (debut+nombre)>liste.size()?liste.size():(debut+nombre);
        for (int i = debut; i < taille ; i++) {
            listeNouvel.add(liste.get(i));
        }
        return listeNouvel;

    }
}